//
//  DetailViewController.swift
//  TrailPOC
//
//  Created by gargam on 18/03/2022.
//

import UIKit
import MapKit

class DetailViewController:UIViewController {
    var trail :Trail!
    @IBOutlet var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMap()
        if let points = trail.points {
            for case let point as Point in points {
                print("+++++++++++")
                print("\(point.timestamp!)")
                print(point.lat)
                print(point.lon)
            }
        }
    }
    
    private func polyLine() -> MKPolyline {
        guard let points = trail.points else {
            return MKPolyline()
        }
        
        let coords: [CLLocationCoordinate2D] = points.map { point in
            let point = point as! Point
            return CLLocationCoordinate2D(latitude: point.lat, longitude: point.lon)
        }
        return MKPolyline(coordinates: coords, count: coords.count)
    }
    
    
    private func loadMap() {
      guard
        let points = trail.points,
        points.count > 0,
        let region = mapRegion()
      else {
          let alert = UIAlertController(title: "Error",
                                        message: "Sorry, this run has no locations saved",
                                        preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel))
          present(alert, animated: true)
          return
      }
        
      mapView.setRegion(region, animated: true)
      mapView.addOverlay(polyLine())
    }
    
    private func mapRegion() -> MKCoordinateRegion? {
        guard
            let points = trail.points,
            points.count > 0
        else {
            return nil
        }
        
        let lats = points.map { point -> Double in
            let point = point as! Point
            return point.lat
        }
        
        let lons = points.map { point -> Double in
            let point = point as! Point
            return point.lon
        }
        
        
        let minLat = lats.min()!
        let maxLat = lats.max()!
        let minLon = lons.min()!
        let maxLon = lons.max()!

        let center = CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2,
                                            longitude: (minLon + maxLon) / 2)
        let span = MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 2,
                                    longitudeDelta: (maxLon - minLon) * 2)
        return MKCoordinateRegion(center: center, span: span)
    }
}

extension DetailViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = .black
        renderer.lineWidth = 5
        return renderer
    }
}
