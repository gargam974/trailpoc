//
//  TrailsViewController.swift
//  TrailPOC
//
//  Created by gargam on 18/03/2022.
//

import UIKit
import CoreData

class TrailsViewController: UIViewController, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    var currentTrail: Trail?
    var trails = [Trail]() {
        didSet {
            updateView()
        }
    }
    private let persistentContainer = CoreDataStack.persistentContainer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
    
    private func updateView() {
//        let hasTrails = trails.count > 0
    }

    // MARK: - NSFetchedResultsControllerDelegate
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Trail> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Trail> = Trail.fetchRequest()

        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]

        // Create Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)

        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self

        return fetchedResultsController
    }()
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let trails = fetchedResultsController.fetchedObjects else { return 0 }
        return trails.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TrailTableViewCell.reuseIdentifier, for: indexPath) as? TrailTableViewCell else {
            fatalError("Error")
        }

        // Fetch Quote
        let trail = fetchedResultsController.object(at: indexPath)

        // Configure Cell
        cell.label.text = "\(trail.timestamp!)"

        return cell
    }
    
    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let trail = self.fetchedResultsController.object(at: indexPath)
        currentTrail = trail
        performSegue(withIdentifier:"showTrailDetail", sender: nil)
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTrailDetail" {
            if let vc = segue.destination as? DetailViewController, let trail = currentTrail {
                vc.trail = trail
            }
        }
    }
    
}
