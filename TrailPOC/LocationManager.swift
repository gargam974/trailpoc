//
//  LocationManager.swift
//  TrailPOC
//
//  Created by gargam on 18/03/2022.
//

import CoreLocation

class LocationManager {
  static let shared = CLLocationManager()
  
  private init() { }
}
