//
//  StartTrailViewController.swift
//  TrailPOC
//
//  Created by gargam on 18/03/2022.
//

import UIKit
import CoreLocation

class StartTrailViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var startStopButton: UIButton!
    var isRecording: Bool = false
    
    private let locationManager = LocationManager.shared
    private var startDate: Date?
    private var timer: Timer?
    private var trail: Trail?
    
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    private var locationList: [CLLocation] = []
    
    
    // MARK: - View Cycle Stuff
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
    }
    
    // MARK: - UI Stuff
    
    func start(){
        isRecording = true
        startStopButton.setTitle("Stop", for: .normal)
        startDate = Date()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            self.durationIncrement()
        }
        startLocationUpdates()
    }
    
    func stop() {
        isRecording = false
        startStopButton.setTitle("Start", for: .normal)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
    }
    
    
    private func showTrail() {
        
    }
    
    
    @IBAction func startAction() {
        if (isRecording) {
            let alertController = UIAlertController(title: "End ?",
                                                    message: "End ?",
                                                    preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            alertController.addAction(UIAlertAction(title: "Save", style: .default) { _ in
                self.stop()
                self.saveTrail()
            })
            present(alertController, animated: true)
        } else {
            self.start()
        }
    }
    
    func durationIncrement() {
        updateUI()
    }
    
    func updateUI() {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        formatter.zeroFormattingBehavior = .pad
        
        guard let startDate = startDate else { return }
        
        let secondsSinceStart = startDate.timeIntervalSinceNow
        let distanceFormatter = MeasurementFormatter()
        
        durationLabel.text = formatter.string(from: TimeInterval(secondsSinceStart))!
        distanceLabel.text = "\(distanceFormatter.string(from: distance))"
        countLabel.text = "\(locationList.count)"
    }
    
    // MARK: - Location stuff
    
    private func startLocationUpdates() {
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.distanceFilter = 6
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("\(locations)")
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            print("\(howRecent)")
            print("\(newLocation.horizontalAccuracy)")
            //            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
            }
            locationList.append(newLocation)
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("PAUSE")
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("RESUME")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        manager.requestAlwaysAuthorization()
    }
    
    private func saveTrail() {
        guard let startDate = startDate else {return}
        let context = CoreDataStack.context
        let trail = Trail(context: context)
        trail.distance = distance.value
        trail.timestamp = startDate
        trail.duration = Int16(startDate.timeIntervalSinceNow)
        
        for location in locationList {
            let pointObject = Point(context: context)
            pointObject.timestamp = location.timestamp
            pointObject.lat = location.coordinate.latitude
            pointObject.lon = location.coordinate.longitude
            trail.addToPoints(pointObject)
        }
        
        CoreDataStack.saveContext()
    }
    
}
