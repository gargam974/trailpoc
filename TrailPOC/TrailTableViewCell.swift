//
//  TrailTableViewCell.swift
//  TrailPOC
//
//  Created by gargam on 18/03/2022.
//

import UIKit

class TrailTableViewCell: UITableViewCell {

    static let reuseIdentifier = "TrailCell"

    @IBOutlet var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
